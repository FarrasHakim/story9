var src1 = "data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9JzMwMHB4JyB3aWR0aD0nMzAwcHgnICBmaWxsPSIjMDAwMDAwIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTAwIDEwMDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwb2x5Z29uIHN0eWxlPSIiIHBvaW50cz0iNjkuMTc4LDYxLjMyOCAxMDAsMzcuNSA2Mi41LDM3LjUgNTAsMCAzNy41LDM3LjUgMCwzNy41IDMwLjg1OSw2MS4yMyAxOC43NSwxMDAgICAgNTAuMDQ5LDc2LjAwMSA4MS4yNzQsMTAwICAiIGZpbGw9IiMwMDAwMDAiPjwvcG9seWdvbj48L2c+PC9zdmc+"
var src2 = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Emojione_2B50.svg/512px-Emojione_2B50.svg.png"
var xhttp = new XMLHttpRequest();
var array;
xhttp.open("GET", "/data/", true);
xhttp.send();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200)
        {
            // console.log(this.response);
            var parsed = JSON.parse(this.response);
            var temp = "";
            array = parsed.items;
            for (var i = 0; i < array.length; i++) {
                temp += "<tr>" + "<th scope='row'>"+ (i+1) +"</th>"+
                        "<td>"+array[i].volumeInfo.title+"</td>"+
                        "<td><img class='img-fluid img-thumbnail' src='"+array[i].volumeInfo.imageLinks.thumbnail+"'></td>"+
                        "<td><img height='50px' class='btn ' onclick='addFav("+ (i+1) +")' src='"+ src1 +"' id='favke"+ (i+1) +"'></td>"+
                        "</tr>";

            }
            document.getElementById('tablooks').innerHTML = temp;
        }
};

var counter = 0;

function addFav(id) {
    if (document.getElementById('favke'+id).src == src1) {
        document.getElementById('favke'+id).src = src2;
        counter ++;
        document.getElementById('counter').innerHTML = counter;
    } else {
        document.getElementById('favke'+id).src = src1;
        counter --;
        document.getElementById('counter').innerHTML = counter;
    }
};
