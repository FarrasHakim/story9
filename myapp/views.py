from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
import json
import urllib.request


def datas(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
    req = urllib.request.Request(url)

    r = urllib.request.urlopen(req).read()
    my_dict = json.loads(r.decode('utf-8'))
    return JsonResponse(my_dict)


def searched(request, search):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + search
    req = urllib.request.Request(url)

    r = urllib.request.urlopen(req).read()
    my_dict = json.loads(r.decode('utf-8'))

    return JsonResponse(my_dict)


def addCounter(request):
    request.session['books'] = request.GET['counter']
    return HttpResponse("mantul")


def index(request):
    print(dict(request.session))
    context = {}
    if request.session.get('books') != None and request.session.get('_auth_user_id') != None:
        context['bukus'] = request.session['books']
    return render(request, "landing.html", context)
