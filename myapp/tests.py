from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from .views import index, datas, searched
import time


# class Story9FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         firefox_options = Options()
#         # firefox_options.add_argument('--headless')
#         self.browser = webdriver.Firefox(firefox_options=firefox_options)

#     def tearDown(self):
#         self.browser.quit()

#     def test_add_favorite(self):
#         self.browser.get(self.live_server_url)
#         time.sleep(4)
#         counter_before = self.browser.find_element_by_id('counter')
#         print(counter_before.text)
#         time.sleep(5)
#         btn_fav = self.browser.find_element_by_id('favke1')
#         btn_fav.send_keys(Keys.RETURN)
#         counter_after = self.browser.find_element_by_id('counter')
#         self.assertEqual(int(counter_after.text), int(counter_before.text) + 1)
#         self.tearDown()


class Story9UnitTest(TestCase):
    def test_url_landing_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)

    def test_function_index(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, index)

    def test_template(self):
        response = Client().get('/landing/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_url_data_is_exist(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code, 200)

    def test_function_data(self):
        found = resolve('/data/')
        self.assertEqual(found.func, datas)

    def test_url_search_data_exist(self):
        search = "anime"
        response = Client().get('/search/' + search)
        self.assertEqual(response.status_code, 200)

    def test_function_search_data(self):
        search = "anime"
        found = resolve('/search/' + search)
        self.assertEqual(found.func, searched)
